package com.example.zhangNicerShop;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.example.zhangNicerShop.model.computer;
import android.view.View;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;

public class MenuActivity extends AppCompatActivity {
    //Initializing variables and list
    private final LinkedList<computer> computersList = new LinkedList<>();
    private int shippingFee = 10;
    private RecyclerView pRecyclerView;
    private Adapter pAdapter;
    private double GSTTax = 0.05;
    private double QSTTax = 0.09975;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //setting the on click and alert dialog
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder myAlertBuilder = new
                        AlertDialog.Builder(MenuActivity.this);

                final String[] singleChoice = getResources().getStringArray(R.array.singleChoice);
                myAlertBuilder.setTitle("Choose your delivery method");

                //choosing the shipping fee
                myAlertBuilder.setSingleChoiceItems(singleChoice, 1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(),singleChoice[i] + " has been selected", Toast.LENGTH_SHORT).show();
                        switch (i) {
                            case 0:
                                shippingFee = 50;
                                break;
                            case 1:
                                shippingFee = 10;
                                break;
                            case 2:
                                shippingFee = 0;
                                break;
                        }
                    }
                });
                myAlertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        NumberFormat numberFormat = new DecimalFormat("##.00");

                        //Initializing Intent
                        Intent intent = new Intent(MenuActivity.this, CheckoutActivity.class);

                        //Formatting and saving into strings
                        String beforeTax = numberFormat.format(calculateBeforeTax());
                        String GST = numberFormat.format(calculateGST());
                        String QST = numberFormat.format(calculateQST());
                        String total = numberFormat.format(calculateTotal());
                        String shipping = numberFormat.format(shippingFee);

                        //Saving the strings into the intent
                        intent.putExtra("beforeTax", beforeTax);
                        intent.putExtra("GST", GST);
                        intent.putExtra("QST", QST);
                        intent.putExtra("total", total);
                        intent.putExtra("shipping", shipping);


                        //Sending all the data
                        startActivity(intent);

                    }
                });
                myAlertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(), "Pressed Cancel",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                myAlertBuilder.show();
            }
        });

        //Initializing computer objects and implementing into the list
        computer c1 = new computer(getResources().getString(R.string.computer01Name),
                getResources().getString(R.string.computer01Price),
                getResources().getDrawable(R.drawable.computer01),
                getResources().getString(R.string.computer01Description),
                getResources().getString(R.string.computer1_quantity),
                getResources().getString(R.string.computer1_subtotal),
                getResources().getString(R.string.addButtonComputer01),
                getResources().getString(R.string.removeButtonComputer01));
        computer c2 = new computer(getResources().getString(R.string.computer02Name),
                getResources().getString(R.string.computer02Price),
                getResources().getDrawable(R.drawable.computer02),
                getResources().getString(R.string.computer02Description),
                getResources().getString(R.string.computer2_quantity),
                getResources().getString(R.string.computer2_subtotal),
                getResources().getString(R.string.addButtonComputer02),
                getResources().getString(R.string.removeButtonComputer02));
        computer c3 = new computer(getResources().getString(R.string.computer03Name),
                getResources().getString(R.string.computer03Price),
                getResources().getDrawable(R.drawable.computer03),
                getResources().getString(R.string.computer03Description),
                getResources().getString(R.string.computer3_quantity),
                getResources().getString(R.string.computer3_subtotal),
                getResources().getString(R.string.addButtonComputer03),
                getResources().getString(R.string.removeButtonComputer03));

        computersList.add(c1);
        computersList.add(c2);
        computersList.add(c3);

        pRecyclerView = findViewById(R.id.recyclerview);

        pAdapter = new Adapter(computersList, this);

        pRecyclerView.setAdapter(pAdapter);

        pRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //If instance was saved set the Variables from the saved Instance
        if (savedInstanceState != null) {
            //Save the instance if there was no change when you rotate back to original position
            computersList.get(0).setQuantity(Integer.toString(savedInstanceState.getInt("computer01Quantity")));
            computersList.get(0).setSubtotal(Integer.toString(savedInstanceState.getInt("computer01Subtotal")));
            computersList.get(1).setQuantity(Integer.toString(savedInstanceState.getInt("computer02Quantity")));
            computersList.get(1).setSubtotal(Integer.toString(savedInstanceState.getInt("computer02Subtotal")));
            computersList.get(2).setQuantity(Integer.toString(savedInstanceState.getInt("computer03Quantity")));
            computersList.get(2).setSubtotal(Integer.toString(savedInstanceState.getInt("computer03Subtotal")));
        }

    }

    //Method to save the data when the instance state changes
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Saving all the data when there's a change
        if (Integer.parseInt(computersList.get(0).getSubtotal()) > 0 ||
                Integer.parseInt(computersList.get(1).getSubtotal()) > 0
                || Integer.parseInt(computersList.get(2).getSubtotal()) > 0) {
            outState.putInt("computer01Quantity", Integer.parseInt(computersList.get(0).getQuantity()));
            outState.putInt("computer01Subtotal", Integer.parseInt(computersList.get(0).getSubtotal()));
            outState.putInt("computer02Quantity", Integer.parseInt(computersList.get(1).getQuantity()));
            outState.putInt("computer02Subtotal", Integer.parseInt(computersList.get(1).getSubtotal()));
            outState.putInt("computer03Quantity", Integer.parseInt(computersList.get(2).getQuantity()));
            outState.putInt("computer03Subtotal", Integer.parseInt(computersList.get(2).getSubtotal()));

        }
    }


    /**Adding all the subtotals and quantities
     *   @return the total before tax
     */
    public double calculateBeforeTax() {
        return  Integer.parseInt(computersList.get(0).getSubtotal())
                + Integer.parseInt(computersList.get(1).getSubtotal())
                + Integer.parseInt(computersList.get(2).getSubtotal());
    }

    /**Calculating GST Tax
     *   @return the GST Tax
     */
    public double calculateGST() {
        return calculateBeforeTax() * GSTTax;
    }

    /**Calculating QST Tax
     *   @return the QST Tax
     */
    public double calculateQST() {
        return calculateBeforeTax() * QSTTax;
    }

    /**Calculating total
     *   @return the total after tax
     */
    public double calculateTotal() {
        return calculateBeforeTax() + calculateGST() + calculateQST() + shippingFee;
    }

}
