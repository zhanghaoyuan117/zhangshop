package com.example.zhangNicerShop.model;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

//Creating the computer class model
public class computer {
    //Initializing the variables
    private String title;
    private String price;
    private Drawable image;
    private String description;
    private String quantity;
    private String subtotal;
    private String addButton;
    private String removeButton;

    /**
     * Computer constructor
     *
     * @param title
     * @param price
     * @param image
     * @param description
     * @param quantity
     * @param subtotal
     * @param addButton
     * @param removeButton
     */
    public computer(String title, String price, Drawable image, String description, String quantity, String subtotal, String addButton, String removeButton) {
        this.title = title;
        this.price = price;
        this.image = image;
        this.description = description;
        this.quantity = quantity;
        this.subtotal = subtotal;
        this.addButton = addButton;
        this.removeButton = removeButton;
    }

    //Computer getters and setters

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getAddButton() {
        return addButton;
    }

    public void setAddButton(String addButton) {
        this.addButton = addButton;
    }

    public String getRemoveButton() {
        return removeButton;
    }

    public void setRemoveButton(String removeButton) {
        this.removeButton = removeButton;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
