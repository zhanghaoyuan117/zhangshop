package com.example.zhangNicerShop;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zhangNicerShop.model.computer;

import java.util.LinkedList;

public class Adapter extends RecyclerView.Adapter<Adapter.ProductViewHolder> {
    private final LinkedList<computer> computersList;
    private LayoutInflater pInflater;


    //Adapter Constructor
    public Adapter(LinkedList<computer> computersList, Context context) {
        this.computersList = computersList;
        pInflater = LayoutInflater.from(context);
    }

    /**
     * Initializing the product viewHolder
     */
    class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView title;
        public TextView price;
        public ImageView image;
        public TextView description;
        public TextView quantity;
        public TextView subtotal;
        public Button addButton;
        public Button removeButton;

        final Adapter pAdapter;

        /**
         *enter views into the adapter
         *
         * @param itemView
         * @param pAdapter
         */
        public ProductViewHolder (@NonNull View itemView, Adapter pAdapter) {
            super(itemView);
            title = itemView.findViewById(R.id.computerTitle);
            price = itemView.findViewById(R.id.computerPrice);
            image = itemView.findViewById(R.id.computerImage);
            description = itemView.findViewById(R.id.computerDescription);
            quantity = itemView.findViewById(R.id.computerQuantity);
            subtotal = itemView.findViewById(R.id.computerSubtotal);
            addButton = itemView.findViewById(R.id.addButtonComputer);
            removeButton = itemView.findViewById(R.id.removeButtonComputer);

            this.pAdapter = pAdapter;
            addButton.setOnClickListener(this);
            removeButton.setOnClickListener(this);
        }

        /**
         * Onclick for both buttons of every layout
         *
         * @param view
         */
        @Override
        public void onClick(View view) {
            int i = getLayoutPosition();
            computer compClicked = computersList.get(i);
            int quant = Integer.parseInt(compClicked.getQuantity());
            int sub = Integer.parseInt(compClicked.getSubtotal());
            switch (view.getId()) {
            case R.id.addButtonComputer:
                quant++;
                sub += Integer.parseInt(compClicked.getPrice());
                compClicked.setQuantity(Integer.toString(quant));
                compClicked.setSubtotal(Integer.toString(sub));
                quantity.setText(Integer.toString(quant));
                subtotal.setText(Integer.toString(sub));
            break;
            case R.id.removeButtonComputer:
                if (quant != 0 && sub != 0) {
                    quant--;
                    sub -= Integer.parseInt(compClicked.getPrice());
                    compClicked.setQuantity(Integer.toString(quant));
                    compClicked.setSubtotal(Integer.toString(sub));
                    quantity.setText(Integer.toString(quant));
                    subtotal.setText(Integer.toString(sub));
                }
                break;
            }
        }
    }

    /**
     *inflate the item layout and return a ViewHolder object
     *
     * @param parent
     * @param i
     * @return
     */
    @NonNull
    @Override
    public Adapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = pInflater.inflate(R.layout.computer,
                parent, false);
        return new ProductViewHolder(itemView, this);
    }

    /**
     *set the view holder attributes
     *
     * @param productViewHolder
     * @param i
     */
    @Override
    public void onBindViewHolder(@NonNull Adapter.ProductViewHolder productViewHolder, final int i) {
        computer computer = computersList.get(i);

        TextView title = productViewHolder.title;
        title.setText(computer.getTitle());
        TextView price = productViewHolder.price;
        price.setText(computer.getPrice());
        ImageView image = productViewHolder.image;
        image.setImageDrawable(computer.getImage());
        TextView desc = productViewHolder.description;
        desc.setText(computer.getDescription());
        TextView quantity = productViewHolder.quantity;
        quantity.setText(computer.getQuantity());
        TextView subtotal = productViewHolder.subtotal;
        subtotal.setText(computer.getSubtotal());
        Button addButton = productViewHolder.addButton;
        addButton.setText(computer.getAddButton());
        Button removeButton = productViewHolder.removeButton;
        removeButton.setText(computer.getRemoveButton());
    }

    /**
     * Size Counter
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return computersList.size();
    }
}

