package com.example.zhangNicerShop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class CheckoutActivity extends AppCompatActivity {
    //Initializing Strings to store the data
    private String beforeTaxString;
    private String GSTString;
    private String QSTString;
    private String totalString;
    private String shippingString;

    //Initializing the TextViews
    private TextView beforeTax;
    private TextView GST;
    private TextView QST;
    private TextView total;
    private TextView shipping;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        //Getting the intent sent from the menu
        Intent intent = getIntent();

        //Locating the TextViews
        beforeTax = findViewById(R.id.totalBeforeTax);
        GST = findViewById(R.id.GSTTax);
        QST = findViewById(R.id.QSTTax);
        total = findViewById(R.id.totalAfterTax);
        shipping = findViewById(R.id.shippingFeeAmount);

        //Getting the Strings from the intent
        beforeTaxString = intent.getStringExtra("beforeTax");
        GSTString = intent.getStringExtra("GST");
        QSTString = intent.getStringExtra("QST");
        totalString = intent.getStringExtra("total");
        shippingString = intent.getStringExtra("shipping");

        //Setting the String into the TextViews
        beforeTax.setText(intent.getStringExtra("beforeTax"));
        GST.setText(intent.getStringExtra("GST"));
        QST.setText(intent.getStringExtra("QST"));
        total.setText(intent.getStringExtra("total"));
        shipping.setText(intent.getStringExtra("shipping"));

        //If there was changes to the instance state
        if (savedInstanceState != null) {
            beforeTax.setText(savedInstanceState.getString("beforeTax"));
            GST.setText(savedInstanceState.getString("GST"));
            QST.setText(savedInstanceState.getString("QST"));
            total.setText(savedInstanceState.getString("total"));
            shipping.setText(savedInstanceState.getString("shipping"));
        }
    }

    //Method to save the data when the instance state changes
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Getting the intent
        Intent intent = getIntent();

        //Saving the state of the data
        outState.putString("beforeTax", beforeTaxString);
        outState.putString("GST", GSTString);
        outState.putString("QST", QSTString);
        outState.putString("total", totalString);
        outState.putString("shipping", shippingString);
    }
}
