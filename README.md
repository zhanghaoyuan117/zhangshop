# Zhang's Shop

Foobar is a Python library for dealing with word pluralization.

## Open project

1. Extract all Files from .zip folder
2. Open Android Studio
3. Click on file --> open --> the directory of the extracted .zip folder
4. Press Shift + F10 to run the app

## Guide

Once you have run the app click the button to go to the Menu page.
In the menu page, you can add items to your cart.
Once you finished, you can click the floating button to choose the shipping fee and proceed to checkout.

## Usage

1. Add items into cart
2. Remove items into cart
3. App navigation
4. Display subtotal
5. Display total amount and taxes in the checkout page
6. Choose shipping fee

## Menu Ideas

1. A toolbar can be used to navigate between different categories (if included different products in many pages)
2. Tab Navigation between different products

## Virtual Device Used

Nexus 1S API 28

## License
[MIT](https://choosealicense.com/licenses/mit/)